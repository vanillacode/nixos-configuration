{ config, lib, pkgs, ... }: {
  services.sonarr.enable = true;
  services.radarr.enable = true;
  services.lidarr.enable = true;
  services.jellyfin = {
    enable = true;
    openFirewall = true;
  };
  environment.systemPackages = with pkgs; [
    jellyfin
    jellyfin-web
    jellyfin-ffmpeg
  ];
}
