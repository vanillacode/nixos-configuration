{
  config,
  pkgs,
  pkgs-unstable,
  lib,
  ...
}:
{
  fonts = {
    packages = with pkgs-unstable; [
      nerd-fonts.iosevka
      nerd-fonts.iosevka-term
      nerd-fonts.hack
      nerd-fonts.mononoki
      nerd-fonts.caskaydia-cove
      nerd-fonts.monaspace
      nerd-fonts.sauce-code-pro
      nerd-fonts.ubuntu-mono
      # monaspace
      ubuntu_font_family
    ];
    fontDir.enable = true;
    fontconfig = {
      cache32Bit = true;
      useEmbeddedBitmaps = true;
    };
  };
  system.fsPackages = [ pkgs.bindfs ];
  fileSystems =
    let
      mkRoSymBind = path: {
        device = path;
        fsType = "fuse.bindfs";
        options = [
          "ro"
          "resolve-symlinks"
          "x-gvfs-hide"
        ];
      };
      aggregatedIcons = pkgs.buildEnv {
        name = "system-icons";
        paths = with pkgs; [
          #libsForQt5.breeze-qt5  # for plasma
          gnome-themes-extra
        ];
        pathsToLink = [ "/share/icons" ];
      };
      aggregatedFonts = pkgs.buildEnv {
        name = "system-fonts";
        paths = config.fonts.packages;
        pathsToLink = [ "/share/fonts" ];
      };
    in
    {
      "/usr/share/icons" = mkRoSymBind "${aggregatedIcons}/share/icons";
      "/usr/local/share/fonts" = mkRoSymBind "${aggregatedFonts}/share/fonts";
    };

}
