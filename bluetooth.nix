{
  ...
}:
{
  hardware.bluetooth.enable = true;
  # hardware.bluetooth.powerOnBoot = true;
  services.blueman.enable = true;

  services.pipewire.wireplumber.extraConfig."51-mitigate-annoying-profile-switch.conf" = {
    "monitor.bluez.properties" = {
      "bluez5.roles" = [
        "a2dp_sink"
        "a2dp_source"
      ];
    };
  };
}
