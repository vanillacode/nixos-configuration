{ config, pkgs, ... }:
{
  services.flatpak = {
    enable = true;
    uninstallUnmanaged = true;
  };
  services.flatpak.update.auto = {
    enable = true;
    onCalendar = "weekly"; # Default value
  };
  services.flatpak.packages = [
    "com.github.tchx84.Flatseal"
    "org.gtk.Gtk3theme.adw-gtk3-dark"
    "io.github.flattool.Warehouse"
    "org.jdownloader.JDownloader"
    "com.github.d4nj1.tlpui"
    # "org.gtk.Gtk3theme.adw-gtk3"
    # "com.github.johnfactotum.Foliate"
    # "org.kde.KStyle.Kvantum//6.6"
    # "org.kde.WaylandDecoration.QAdwaitaDecorations//6.6"
  ];
}
