{
  pkgs-unstable,
  pkgs,
  config,
  lib,
  ...
}:
{
  programs.uwsm = {
    enable = true;
    waylandCompositors.hyprland = {
      binPath = "/run/current-system/sw/bin/Hyprland";
      comment = "Hyprland session managed by uwsm";
      prettyName = "Hyprland";
    };
  };
  environment.systemPackages = with pkgs; [
    hyprland
    pyprland
    hyprlock
    hypridle
    hyprpaper
    hyprshade
    waybar
    wofi
  ];
}
