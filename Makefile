.PHONY: switch
switch:
	nixos-rebuild switch --flake .

.PHONY: update
update:
	nix flake update

.PHONY: clean
clean:
	nix-collect-garbage -d
