{
  description = "My Configuration";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    auto-cpufreq = {
      url = "github:AdnanHodzic/auto-cpufreq";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-flatpak.url = "github:gmodena/nix-flatpak/?ref=v0.4.1";
    hyprland.url = "github:hyprwm/Hyprland";
  };

  outputs =
    {
      self,
      nixpkgs,
      home-manager,
      nix-flatpak,
      flake-utils,
      auto-cpufreq,
      ...
    }@inputs:
    let
      lib = nixpkgs.lib;
      system = "x86_64-linux";
    in
    {
      nixosConfigurations = {
        nixos = lib.nixosSystem {
          inherit system;
          specialArgs = {
            # To use packages from nixpkgs-stable,
            # we configure some parameters for it first
            pkgs-unstable = import inputs.nixpkgs-unstable {
              inherit system;
              config.allowUnfree = true;
            };
            inherit inputs;
          };
          modules = [
            nix-flatpak.nixosModules.nix-flatpak
            home-manager.nixosModules.home-manager
            ./configuration.nix
            ./fonts.nix
            ./flatpak.nix
            ./hyprland.nix
            ./bluetooth.nix
            ./lsp.nix
            ./syncthing.nix
            ./laptop-power-management.nix
            # ./wireguard.nix
            # ./tailscale.nix
            # ./media.nix
            ./gaming.nix
            auto-cpufreq.nixosModules.default
            # ./nextcloud.nix
            # {
            #   home-manager = {
            #     users.nixos = import ./home.nix;
            #     useGlobalPkgs = true;
            #   };
            # }
          ];
        };
      };
    };
}
