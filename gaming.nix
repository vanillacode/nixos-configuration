{
  pkgs-unstable,
  pkgs,
  config,
  lib,
  ...
}:
{
  # services.sunshine = {
  #   enable = true;
  #   autoStart = true;
  #   capSysAdmin = true;
  #   openFirewall = true;
  # };
  programs = {
    java.enable = true;
    gamemode.enable = true;
    gamescope = {
      enable = true;
      # capSysNice = true;
    };
    steam = {
      # enable = true;
      # gamescopeSession.enable = true;
    };
  };
  environment.systemPackages = with pkgs-unstable; [
    # gamescope
    mangohud
    vkbasalt
    retroarch-assets
    libretro.citra
    libretro.dolphin
    libretro.mgba
    retroarch
    heroic
    bottles
    (lutris.override {
      extraLibraries = pkgs: [
        # List library dependencies here
      ];
    })
    sc-controller
    antimicrox
    rpcs3
    cemu
  ];
  boot.kernelModules = [ "uinput" ];
  services.udev.extraRules = ''
    # PS5 DualSense controller over USB hidraw
    KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="0ce6", MODE="0666", TAG+="uaccess"

    # PS5 DualSense controller over bluetooth hidraw
    KERNEL=="hidraw*", KERNELS=="*054C:0CE6*", MODE="0666", TAG+="uaccess"

    ACTION=="add", SUBSYSTEM=="input", ENV{ID_PATH}!="platform-sound", RUN+="${pkgs.input-remapper}/bin/input-remapper-control --command autoload --device $env{DEVNAME}"
  '';
  services.flatpak.packages = [
    "com.steamgriddb.steam-rom-manager"
    # "com.usebottles.bottles"
    # "info.cemu.Cemu"
    # "com.heroicgameslauncher.hgl"
    # "com.valvesoftware.Steam"
    # "org.freedesktop.Platform.VulkanLayer.gamescope/x86_64/24.08"
  ];
  services.input-remapper = {
    enable = true;
  };
  # services.displayManager = {
  #   enable = true;
  #   sessionPackages = [
  #     (pkgs.writeTextFile {
  #       name = "gamescope-steam";
  #       text = ''
  #         [Desktop Entry]
  #         Name=Steam (Gamescope)
  #         Exec=/home/nixos/gs.sh
  #         Type=Application
  #       '';
  #       destination = "/share/wayland-sessions/gamescope-steam.desktop";
  #       derivationArgs = {
  #         passthru.providedSessions = [ "gamescope-steam" ];
  #       };
  #     })
  #   ];
  # };
  # programs.uwsm = {
  #   enable = true;
  #   waylandCompositors.steam = {
  #     binPath = "/home/nixos/gs.sh";
  #     comment = "Steam session managed by uwsm";
  #     prettyName = "Steam";
  #   };
  # };
}
