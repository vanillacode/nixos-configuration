{ config, pkgs, pkgs-unstable, ... }: rec {
  services.nextcloud = {
    enable = true;
    package = pkgs-unstable.nextcloud28;
    hostName = "192.168.122.1";
    config = {
      adminpassFile = "/etc/nextcloud-admin-pass";
      extraTrustedDomains = [ "192.168.122.143" ];
    };
  };
}
