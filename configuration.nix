{
  config,
  pkgs,
  pkgs-unstable,
  ...
}:
{
  imports = [ ./hardware-configuration.nix ];

  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  networking.hostName = "nixos";
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  networking.networkmanager.enable = true;

  time.timeZone = "Asia/Kolkata";
  i18n.defaultLocale = "en_US.UTF8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF8";
    LC_IDENTIFICATION = "en_US.UTF8";
    LC_MEASUREMENT = "en_US.UTF8";
    LC_MONETARY = "en_US.UTF8";
    LC_NAME = "en_US.UTF8";
    LC_NUMERIC = "en_US.UTF8";
    LC_PAPER = "en_US.UTF8";
    LC_TELEPHONE = "en_US.UTF8";
    LC_TIME = "en_US.UTF8";
  };

  security.doas.enable = true;
  security.sudo.enable = false;
  security.doas.extraRules = [
    {
      users = [ "nixos" ];
      keepEnv = true;
      persist = true;
    }
  ];

  services.cron = {
    enable = true;
    systemCronJobs = [
      "*/30 * * * * nixos tldr --update"
      "0 15 * * Fri nixos nix-store --optimize"
    ];
  };
  xdg.portal.enable = true;
  xdg.portal.config = {
    common = {
      default = [ "gtk" ];
    };
  };
  xdg.portal.extraPortals = [
    pkgs.xdg-desktop-portal-gtk
    pkgs.xdg-desktop-portal-kde
  ];
  services.greetd = {
    enable = true;
    vt = 2;
    settings = {
      initial_session = {
        command = "${pkgs.uwsm}/bin/uwsm start -S -F /run/current-system/sw/bin/Hyprland";
        user = "nixos";
      };
      default_session = {
        command = "${pkgs.cage}/bin/cage -s -mlast -- ${pkgs.greetd.regreet}/bin/regreet";
        user = "nixos";
      };
    };
  };
  services.xserver = {
    enable = true;
    # resolutions = [
    #   {
    #     x = 1920;
    #     y = 1080;
    #   }
    # ];
    # windowManager.bspwm.enable = true;
    xkb = {
      layout = "us";
      variant = "";
    };
  };
  services.printing.enable = true;
  services.locate = {
    enable = true;
    package = pkgs.mlocate;
    interval = "hourly";
    localuser = null;
  };

  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  users.users.nixos = {
    shell = pkgs.fish;
    isNormalUser = true;
    description = "nixos";
    extraGroups = [
      "video"
      "networkmanager"
      "wheel"
      "mlocate"
      "input"
      "autologin"
    ];
    packages = with pkgs; [ firefox ];
  };

  nixpkgs.config.allowUnfree = true;

  services.interception-tools = {
    enable = true;
    plugins = with pkgs; [ interception-tools-plugins.caps2esc ];
    udevmonConfig = ''
      - JOB: "${pkgs.interception-tools}/bin/intercept -g $DEVNODE | ${pkgs.interception-tools-plugins.caps2esc}/bin/caps2esc -m 1 | ${pkgs.interception-tools}/bin/uinput -d $DEVNODE"
        DEVICE:
          EVENTS:
            EV_KEY: [KEY_CAPSLOCK, KEY_ESC]
    '';
  };

  environment.systemPackages = with pkgs; [
    nodejs
    xdotool
    age
    python3
    aria2
    ddcutil
    doas-sudo-shim
    pkgs-unstable.neovim
    bspwm
    eza
    fd
    bat
    skim
    fzf
    git
    kitty
    sxhkd
    home-manager
    dunst
    pulsemixer
    stow
    sshfs
    pyenv
    restic
    rclone
    xwallpaper
    polybar
    ripgrep
    libnotify
    xclip
    wl-clipboard-x11
    dmenu
    croc
    file
    killall
    handlr
    lazygit
    stdenv
    llvmPackages.bintools
    gcc
    gnumake
    wget
    gnugrep
    nix-search-cli
    mlocate
    unzip
    tealdeer
    pamixer
    tmux
    dconf
    dconf-editor
    archivemount
    adwaita-icon-theme
    adw-gtk3
    pulsemixer
    unrar-free
    rustup
    # cage
    # greetd.greetd
    # greetd.regreet
    qt6Packages.qtstyleplugin-kvantum
    # libsForQt5.qtstyleplugin-kvantum
    # lxappearance
    # breeze-gtk
    # breeze-icons
  ];

  programs.fish.enable = true;
  programs.light.enable = true;
  programs.direnv.enable = true;
  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [
    # Add any missing dynamic libraries for unpackaged programs
    # here, NOT in environment.systemPackages
  ];
  programs.appimage = {
    enable = true;
    binfmt = true;
  };
  programs.kdeconnect.enable = true;

  system.stateVersion = "23.11";

  nix.settings.experimental-features = [
    "flakes"
    "nix-command"
  ];
}
